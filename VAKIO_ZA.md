#All
https://restcountries.eu/rest/v2/all

https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;alpha3Code;capital;region;subregion;population;latlng;currencies;flag

#Search by country name
https://restcountries.eu/rest/v2/name/united

#Search by region: Africa, Americas, Asia, Europe, Oceania
https://restcountries.eu/rest/v2/region/europe
